'use strict';

appScripts['events'] = (() => {

	let restapi;
	let app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			Promise.all([restapi.request(restapi.authLinks().EVENTS, {query: {limit: 25}}), restapi.request(restapi.authLinks().SKILLS)]).then((reqs) => {
				const events = reqs[0].responseJSON;
				const skills = reqs[1].responseJSON;
				const objSkills = utils.arrayToObject('uuid', skills);

				const labels = {
					POSTE: 'Poste de secours',
					FORMATION: 'Formation',
					OTHER: 'Autre'
				};

				let html = '<table>' +
					'<thead>' +
						'<td>Type</td>' +
						'<td>Nom</td>' +
						'<td>Lieu</td>' +
						'<td>Horaires</td>' +
						'<td>Status</td>' +
						'<td>Besoins</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in events) {
					const event = events[key];

					html +=	'<tr>';
					html += '<td><span class="tag ' + event.type.toLowerCase() + '">' + labels[event.type] + '</span></td>';
					html += '<td>' + event.name + '</td>';
					html += '<td>' + (event.place? event.place : '-') + '</td>';

					const beginDate = new Date(event.beginDate*1000);
					const endDate = new Date(event.endDate*1000);
					const isSameDay = utils.isSameDay(beginDate, endDate);
					const isSameYear = new Date().getFullYear() === beginDate.getFullYear();
					const dateModel = isSameYear? '%j %F' : '%j %F %Y';
					const beginDateModel = isSameDay? '%l <b>' + dateModel + '</b> de <b>%Gh%i</b>' : 'du %l <b>' + dateModel + '</b> à <b>%Gh%i</b>';
					const endDateModel = isSameDay? 'à <b>%Gh%i</b>' : 'au %l <b>%j %F %Y</b> à <b>%Gh%i</b>';
					html += '<td>' + utils.dateToString(beginDateModel, beginDate) + ' ' + utils.dateToString(endDateModel, endDate) + '</td>';

					html += '<td><span class="tag ' + (event.isOpen? 'ok">Ouvert' : 'not-ok">Fermé') + '</span></td>';

					html += '<td>';

					const staffQuantity = event.staff? event.staff.length : 0;
					html += '<span class="tag ' + (!event.requiredQuantity || staffQuantity >= event.requiredQuantity? 'ok' : 'not-ok' ) + '">' + staffQuantity + (event.requiredQuantity? '/' + event.requiredQuantity : '' ) + '</span>';

					try {
						const requirements = {};
					  if(event.skillsRequirement) {

					    const allSkillsTreeEntries = [];

					    /*
					      I redo skillsRequirement with specifics rows
					    */
					    for(let key in event.skillsRequirement) {
					      const requirement = event.skillsRequirement[key];
					      requirements[requirement.skill.uuid] = {
					        requiredQuantity: requirement.requiredQuantity,
					        staff: [], // Save staff who have this skill (ok)
					        ghostsStaff: [], // Save staff who have an other fulled skill and can "valid" an other skill
					        availablesStaff: [] // Save staff who have this skills or an other (maybe)
					      };
					      allSkillsTreeEntries.push(requirement.skill.uuid);
					    }

					    /*
					      I fill the skillsRequirement with staff skills
					    */
					    for(let key in event.staff) {
					      const staff = event.staff[key];

					      if(staff.assignedSkill) {
					        if(!requirements[staff.assignedSkill.uuid]) {
					          requirements[staff.assignedSkill.uuid] = {
					            requiredQuantity: 0,
					            staff: [],
					            ghostsStaff: [],
					            availablesStaff: []
					          };
					          allSkillsTreeEntries.push(staff.assignedSkill.uuid);
					        }
					        requirements[staff.assignedSkill.uuid].staff.push(staff.uuid);
					        continue;
					      }

					      for(let key in staff.skills) {
					        const skillUuid = staff.skills[key].uuid;

					        let parent = objSkills[skillUuid];
					        while(parent) {
					          const requirement = requirements[parent.uuid];
					          if(requirement) {
					            if(requirement.staff.indexOf(staff.uuid) === -1) {
					              requirement.staff.push(staff.uuid);
					            }

					            if(parent.uuid !== skillUuid && allSkillsTreeEntries.indexOf(parent.uuid) > -1) {
					              allSkillsTreeEntries.splice(allSkillsTreeEntries.indexOf(parent.uuid), 1);
					            }
					            break;
					          }
					          parent = objSkills[parent.parentUuid];
					        }
					      }
					    }

					    /*
					      I only keep one occurrence of staff per skills tree and save staff skills level
					    */
					    const staffSkillsTopLevel = {};
					    for(let key in allSkillsTreeEntries) {
					      const skillUuid = allSkillsTreeEntries[key];
					      const requirement = requirements[skillUuid];
					      const alreadyCrossedStaff = [];

					      let parent = objSkills[skillUuid];
					      while(parent) {
					        const requirement = requirements[parent.uuid];
					        if(requirement) {
					          for(let i = 0; i < requirement.staff.length; i++) {
					            const staffUuid = requirement.staff[i];
					            if(alreadyCrossedStaff.indexOf(staffUuid) > -1) {
					              requirement.staff.splice(i,1);
					              i--;
												continue;
					            }

					            alreadyCrossedStaff.push(staffUuid);

											if(!staffSkillsTopLevel[staffUuid]) {
					              staffSkillsTopLevel[staffUuid] = [];
					            }
					            if(staffSkillsTopLevel[staffUuid].indexOf(parent.uuid) === -1) {
					              staffSkillsTopLevel[staffUuid].push(parent.uuid);
					            }
					          }

					          if(parent.uuid !== skillUuid && allSkillsTreeEntries.indexOf(parent.uuid) > -1) {
					            allSkillsTreeEntries.splice(allSkillsTreeEntries.indexOf(parent.uuid), 1);
					          }
					        }
					        parent = objSkills[parent.parentUuid];
					      }
					    }

					    /*
					      Now, allSkillsTreeEntries contain only skills tree entries (top level entry)
					    */

					    /*
					      Move duplicated staff to "availables"
					    */
					    for(let staffUuid in staffSkillsTopLevel) {
					      const staffSkills = staffSkillsTopLevel[staffUuid];
					      if(staffSkills.length > 1) {
					        for(let key in staffSkills) {
					          let skillUuid = staffSkills[key];
					          requirements[skillUuid].staff.splice(requirements[skillUuid].staff.indexOf(staffUuid), 1);
					          requirements[skillUuid].availablesStaff.push(staffUuid);
					        }
					      }
					    }

					    /*
					      Down skills
					    */
					    for(let key in allSkillsTreeEntries) {
					      const skillUuid = allSkillsTreeEntries[key];

					      let staffToMove = [];
					      let availableStaffToMove = [];
					      let parent = objSkills[skillUuid];
					      while(parent) {
					        const requirement = requirements[parent.uuid];
					        if(requirement) {

					          const nbStaffToMove = requirement.staff.length - requirement.requiredQuantity;
					          if(nbStaffToMove > 0) {
					            staffToMove = staffToMove.concat(requirement.staff.slice(-nbStaffToMove));
					          }
					          else if(nbStaffToMove < 0) {
					            requirement.ghostsStaff = requirement.ghostsStaff.concat(staffToMove.slice(staffToMove));
					            staffToMove = staffToMove.slice(0, staffToMove);
					          }

					          const nbAvailablesStaffToMove = requirement.staff.length + requirement.availablesStaff.length - requirement.requiredQuantity;
					          if(nbAvailablesStaffToMove > 0) {
					            availableStaffToMove = availableStaffToMove.concat(requirement.availablesStaff.slice(-nbAvailablesStaffToMove));
					            requirement.availablesStaff = requirement.availablesStaff.slice(0, -nbAvailablesStaffToMove);
					          }
					          else if(nbAvailablesStaffToMove < 0) {
					            requirement.availablesStaff = requirement.availablesStaff.concat(availableStaffToMove.slice(nbAvailablesStaffToMove));
					            availableStaffToMove = availableStaffToMove.slice(0, nbAvailablesStaffToMove);
					          }

					        }
					        parent = objSkills[parent.parentUuid];
					      }
					    }
					  }

						if(Object.keys(requirements).length) {
							html += ' dont';

							for(let uuid in requirements) {
								const requirement = requirements[uuid];

						    const staffQuantity = requirement.staff.length;
						    const staffAndGhostsQuantity = staffQuantity + requirement.ghostsStaff.length;
						    const availableQuantity = staffAndGhostsQuantity + requirement.availablesStaff.length;
						    const requiredQuantity = requirement.requiredQuantity;
						    const className = staffAndGhostsQuantity >= requiredQuantity? 'ok' : (availableQuantity >= requiredQuantity? 'maybe' : 'not-ok' );
						    const skillName = objSkills[uuid].name;
						    const skillShortName = objSkills[uuid].shortName;
						    const skillDescription = objSkills[uuid].description;

						    html += ' <span class="tag ' + className + '"><abbr title="' + skillName + (skillDescription? '\n——\n' + skillDescription : '') + '">' + staffQuantity + '/' + requiredQuantity + ' ' + skillShortName + '</abbr></span>';
							}
						}
					} catch(err) {
						console.error(err);
					}
					html += '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});
		}
	}
})();
