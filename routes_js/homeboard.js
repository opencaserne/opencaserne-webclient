'use strict';

appScripts['homeboard'] = (() => {

	let restapi,
		app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {

			restapi.request(restapi.authLinks().NEWS, {query: {limit: 5}}).then((req) => {
				const news = req.responseJSON;

				let html = '';
				for(let key in news) {
					const actu = news[key];

					html += '<div class="appblock-sub">';
							html += '<p>' + actu.content.replace(/\r/g, '<br />') + '</p>';
							html += '<div class="align-right">Le ' + utils.dateToString(utils.dateModel, new Date(actu.creationDate*1000)) + '</div>';
					html += '</div>';
				}

				document.getElementById('news-container').innerHTML = html;
			});

			restapi.request(restapi.authLinks().EVENTS, {query: {participating: true}}).then((req) => {
				const events = req.responseJSON;

				const labels = {
					POSTE: 'Poste de secours',
					FORMATION: 'Formation',
					OTHER: 'Autre'
				};

				let html = '';
				for(let key in events) {
					const event = events[key];

					html += '<tr>';
							html += '<td><span class="tag ' + event.type.toLowerCase() + '">' + labels[event.type] + '</span></td>';
							html += '<td>' + event.name + '</td>';
							html += '<td>' + (event.place? event.place : '-') + '</td>';

							const beginDate = new Date(event.beginDate*1000);
							const endDate = new Date(event.endDate*1000);
							const isSameDay = utils.isSameDay(beginDate, endDate);
							const isSameYear = new Date().getFullYear() === beginDate.getFullYear();
							const dateModel = isSameYear? '%j %F' : '%j %F %Y';
							const beginDateModel = isSameDay? '%l <b>' + dateModel + '</b> de <b>%Gh%i</b>' : 'du %l <b>' + dateModel + '</b> à <b>%Gh%i</b>';
							const endDateModel = isSameDay? 'à <b>%Gh%i</b>' : 'au %l <b>%j %F %Y</b> à <b>%Gh%i</b>';
							html += '<td>' + utils.dateToString(beginDateModel, beginDate) + ' ' + utils.dateToString(endDateModel, endDate) + '</td>';

					html += '</tr>';
				}

				document.getElementById('participation-container').innerHTML = html;
			});

			restapi.request(restapi.authLinks().EVENTS, {query: {can_participate: true, limit: 5}}).then((req) => {
				const events = req.responseJSON;

				const labels = {
					POSTE: 'Poste de secours',
					FORMATION: 'Formation',
					OTHER: 'Autre'
				};

				let html = '';
				for(let key in events) {
					const event = events[key];

					html += '<tr>';
							html += '<td><span class="tag ' + event.type.toLowerCase() + '">' + labels[event.type] + '</span></td>';
							html += '<td>' + event.name + '</td>';
							html += '<td>' + (event.place? event.place : '-') + '</td>';

							const beginDate = new Date(event.beginDate*1000);
							const endDate = new Date(event.endDate*1000);
							const isSameDay = utils.isSameDay(beginDate, endDate);
							const isSameYear = new Date().getFullYear() === beginDate.getFullYear();
							const dateModel = isSameYear? '%j %F' : '%j %F %Y';
							const beginDateModel = isSameDay? '%l <b>' + dateModel + '</b> de <b>%Gh%i</b>' : 'du %l <b>' + dateModel + '</b> à <b>%Gh%i</b>';
							const endDateModel = isSameDay? 'à <b>%Gh%i</b>' : 'au %l <b>%j %F %Y</b> à <b>%Gh%i</b>';
							html += '<td>' + utils.dateToString(beginDateModel, beginDate) + ' ' + utils.dateToString(endDateModel, endDate) + '</td>';

					html += '</tr>';
				}

				document.getElementById('events-container').innerHTML = html;
			});

		}
	}
})();
