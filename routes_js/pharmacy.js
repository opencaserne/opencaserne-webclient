'use strict';

appScripts['pharmacy'] = (() => {

	let restapi,
		app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().PHARMACY, {query: {limit: 25}}).then((req) => {
				const pharmacy = req.responseJSON;

				let html = '<table>' +
					'<thead>' +
						'<td>Nom</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in pharmacy) {
					const obj = pharmacy[key];

					html +=	'<tr>';
					html += '<td>' + obj.name + '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});
		}
	}
})();
