'use strict';

appScripts['vehicles'] = (() => {

	let restapi,
		app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().VEHICLES, {query: {limit: 25}}).then((req) => {
				const vehicles = req.responseJSON;

				let html = '<table>' +
					'<thead>' +
						'<td>Nom</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in vehicles) {
					const vehicle = vehicles[key];

					html +=	'<tr>';
					html += '<td>' + vehicle.name + '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});
		}
	}
})();
