'use strict';

appScripts['skills'] = (() => {

	let restapi;
	let app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().SKILLS, {query: {limit: 25}}).then((req) => {
				const skills = req.responseJSON;

				let html = '<table>' +
					'<thead>' +
						'<td>Nom</td>' +
						'<td>Abbréviation</td>' +
						'<td>Description</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in skills) {
					const skill = skills[key];

					html +=	'<tr>';
					html += '<td>' + '&emsp;'.repeat(skill.level*2) + (skill.level >= 1? '&rdsh; ' : '') + skill.name + '</td>';
					html += '<td><span class="tag">' + skill.shortName + '</span></td>';
					html += '<td>' + (skill.description? skill.description : '-') + '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});

		}
	}
})();
