'use strict';

appScripts['news'] = (() => {

	let restapi;
	let app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().NEWS, {query: {limit: 25}}).then((req) => {
				const news = req.responseJSON;

				let html = '<div class="app-parent">';
				for(let key in news) {
					const actu = news[key];

					html += '<div class="appblock-sub">';
					html += '<p>' + actu.content.replace(/\r/g, '<br />') + '</p>';
					html += '<div class="align-right">Le ' + utils.dateToString(utils.dateModel, new Date(actu.creationDate*1000)) + '</div>';
					html += '</div>';
				}
				html += '</div>';

				document.getElementById('content-container').innerHTML = html;
			});

		}
	}
})();
