'use strict';

appScripts['departments'] = (() => {

	let restapi;
	let app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().DEPARTMENTS, {query: {limit: 25}}).then((req) => {
				const departments = req.responseJSON;

				let html = '<table>' +
					'<thead>' +
						'<td>Nom</td>' +
						'<td>Adresse</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in departments) {
					const department = departments[key];

					html +=	'<tr>';
					html += '<td>' + '&emsp;'.repeat(department.level*2) + (department.level >= 1? '&rdsh; ' : '') + department.name + '</td>';
					html += '<td>' + (department.address? department.address : '-') + '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});
		}
	}
})();
