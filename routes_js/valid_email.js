'use strict';

jsAppRoute = (() => {

	let restapi,
		app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeData, data, uriData) => {
			const waitPopup = popup.wait();

			if(!uriData.key) {
				popup.custom('L\'url est invalide.', '', [{value: 'OK', action: () => { window.location.href='/'; }}], '', false, 'fatal');
				popup.remove(waitPopup);
				return;
			}

			const formData = {
				secureHash: uriData.key
			};

			let formLink = restapi.baseLinks()['VALID_EMAIL'];
			restapi.request(formLink, {body: formData})
			.then(utils.defaultThen)
			.catch(utils.defaultCatch)
			.then(() => {
				app.changeRoute('login').catch(utils.defaultCatch);
				popup.remove(waitPopup);
			});

			return true;
		}
	}
})();
