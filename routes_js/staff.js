'use strict';

appScripts['staff'] = (() => {

	let restapi;
	let app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().STAFF, {query: {limit: 25}}).then((req) => {
				const staff = req.responseJSON;

				const labels = {
					WAITING: { name: 'En attente', tag: '' },
					ENABLED: { name: 'Validé', tag: 'ok' },
					ACTIVATED: { name: 'Activé', tag: 'ok' },
					DISABLED: { name: 'Désactivé', tag: 'not-ok' },
					DELETED: { name: 'Supprimé', tag: 'not-ok' }
				};

				let html = '<table>' +
					'<thead>' +
						'<td>Statut</td>' +
						'<td>Nom</td>' +
						'<td>Prénom</td>' +
						'<td>Section</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in staff) {
					const user = staff[key];

					html +=	'<tr>';
					html += '<td><span class="tag ' + labels[user.status].tag + '">' + labels[user.status].name + '</span></td>';
					html += '<td>' + user.surname + '</td>';
					html += '<td>' + user.name + '</td>';
					html += '<td>' + user.departmentName + '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});

		}
	}
})();
