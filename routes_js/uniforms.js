'use strict';

appScripts['uniforms'] = (() => {

	let restapi,
		app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().UNIFORMS, {query: {limit: 25}}).then((req) => {
				const uniforms = req.responseJSON;

				let html = '<table>' +
					'<thead>' +
						'<td>Nom</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in uniforms) {
					const uniform = uniforms[key];

					html +=	'<tr>';
					html += '<td>' + uniform.name + '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});
		}
	}
})();
