'use strict';

appScripts['login'] = (() => {

	let restapi,
		app;

	let emailEl,
		passwordEl;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			emailEl = document.getElementById('loginFormEmail'),
			passwordEl = document.getElementById('loginFormPassword');
		},
		login: () => {
			const waitPopup = popup.wait();

			const formData = {
				emailOrUsername: emailEl.value,
				password: passwordEl.value
			};

			let error = false;
			if(!formData.emailOrUsername) {
    		emailEl.setCustomValidity('Merci d\'entrer un email');
				error = true;
			}
			else {
    		emailEl.setCustomValidity('');
			}

			if(!formData.password) {
    		passwordEl.setCustomValidity('Merci d\'entrer un mot de passe');
				error = true;
			}
			else {
    		passwordEl.setCustomValidity('');
			}

			if(error) {
				popup.remove(waitPopup);
				return false;
			}

			let formLink = restapi.baseLinks()['LOG_IN'];
			restapi.request(formLink, {body: formData})
			.then(() => app.changeRoute('homeboard'))
			.catch(console.log)
			.then(() => {
				popup.remove(waitPopup);
			});

			return true;
		}
	}
})();
