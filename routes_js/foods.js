'use strict';

appScripts['foods'] = (() => {

	let restapi,
		app;

	return {
		setContext: (r, a) => {
			restapi = r;
			app = a;
		},
		init: (routeId, routeObj, routeData, data, uriData) => {
			document.getElementById('header-container').innerText = routeObj.title;

			restapi.request(restapi.authLinks().FOODS, {query: {limit: 25}}).then((req) => {
				const foods = req.responseJSON;

				let html = '<table>' +
					'<thead>' +
						'<td>Nom</td>' +
					'</thead>' +
					'<tbody class="app-parent">';

				for(let key in foods) {
					const food = foods[key];

					html +=	'<tr>';
					html += '<td>' + food.name + '</td>';
					html += '</tr>';
				}

				html += '</tbody>' +
				'</table>';

				document.getElementById('content-container').innerHTML = html;
			});
		}
	}
})();
