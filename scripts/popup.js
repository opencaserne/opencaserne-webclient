'use strict';

const popup = (function() {

	const popupContainer = utils.createElementFromHTML('<div id="popupContainer"></div>');

	const DEFAULT = 'DEFAULT';

	// ---------- Scope Privé ----------
	function custom(body, title, buttons, drawable, cancelable, type) {

		const popupEl = utils.createElementFromHTML(
			'<div class="popup" data-cancelable="' + cancelable + '" data-type="' + (typeof type == 'string'? type : 'default' ) + '">' +
			'<div class="box">' +
			'<div class="content">' +
			'<div class="drawable">' + drawable + '</div>' +
			'<div class="title">' + (typeof title == 'string'? title : '' ) + '</div>' +
			'<div class="message">' + (typeof body == 'string'? body : '' ) + '</div>' +
			'</div>' +

			'<div class="buttons"></div>' +
			'</div>' +
			'</div>'
		);

		if(typeof body == 'object') {
			popupEl.getElementsByClassName('message')[0].appendChild(body);
		}

		const buttonsArea = popupEl.getElementsByClassName('buttons')[0];
		let focusButton;
		for(const i in buttons) {
			const button = buttons[i];

			const buttonEl = utils.createElementFromHTML('<button class="button">' + button.value + '</button>');
			buttonEl.style.background = ((button.background)? button.background : ((button.action)? '#3389B9' : '#0E0E0E' ) );
			buttonEl.style.color = (button.color? button.color : '#fff');

			if(!focusButton) focusButton = buttonEl;

			buttonEl.addEventListener('click', (event) => {
				event.preventDefault();
				event.stopPropagation();

				let res;
				if(typeof button.action != 'function' || (res = button.action()) === true) {
					popup.remove(popupEl);
				} else if(res === false) {
					utils.playCssAnimationByClass(popupEl, 'popupShake');
				}
			}, false);

			buttonsArea.appendChild(buttonEl);
		}

		popupContainer.appendChild(popupEl);

		setTimeout(function() {
			if(cancelable) {
				popupEl.addEventListener('click', (e) => { remove.bind(this, popupEl); }, false);
			} else {
				popupEl.addEventListener('click', (e) => { utils.playCssAnimationByClass(popupEl, 'popupShake'); }, false);
			}

			if(focusButton) focusButton.focus();
		}, 500);

		utils.playCssAnimationByClass(popupEl, 'in');

		return popupEl;
	}

	function ask(message, title, buttons, drawable) {

		if(typeof message === 'undefined' || message === DEFAULT) message = '?';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-menun"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'OK' }  ];

		return custom(message, title, buttons, drawable, false, 'ask');
	}

	function wait(message, title, buttons, drawable) {

		if(typeof message === 'undefined' || message === DEFAULT) message = 'Veuillez patienter...';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-wait"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [];

		return custom(message, title, buttons, drawable, false, 'wait');
	}

	function success(message, title, buttons, drawable) {

		if(typeof message === 'undefined' || message === DEFAULT) message = 'L\'opération a réussie';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-success"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'OK' } ];

		return custom(message, title, buttons, drawable, true, 'success');
	}

	function error(message, title, buttons, drawable) {

		if(typeof message === 'undefined' || message === DEFAULT) message = 'L\'opération a échouée';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-error"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'OK' } ];

		return custom(message, title, buttons, drawable, true, 'error');
	}

	function fatal(message, title, buttons, drawable) {

		removeAll();

		if(typeof message === 'undefined' || message === DEFAULT) message = 'Une erreur s\'est produite...';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-fatal"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'Accueil', 'background': '#0E0E0E', 'action': () => { window.location.href = '/'; } }, { 'value': 'Recharger', 'action': () => { window.location.reload(); } } ];

		return custom(message, title, buttons, drawable, false, 'fatal');
	}

	function form(title, formDescription, callback) {

		if(typeof title === 'undefined' || title === DEFAULT) title = '';

		const formObject = document.createElement('form');
		formDescription = formDescription || [];
		for(const key in formDescription) {

			const description = formDescription[key];
			let formEl;

			if(description.type == 'datetime-local' && description.value) {
				description.value = new Date(description.value*1000).toISOString();
			}

			let line = '';
			line += (description.value? ' value="' + description.value + '"' : '');
			line += (description.name? ' name="' + description.name + '"' : '');
			line += (description.required? ' required="required"' : '');
			line += (description.pattern? ' pattern="' + description.pattern + '"' : '');
			line += (description.placeholder? ' placeholder="' + description.placeholder + '"' : '');
			switch(description.type) {
				case 'longtext':
				formEl = utils.createElementFromHTML('<div><label>' + (description.label? '<span>' + description.label + '</span>' : '') + '<textarea' + line + '>' + (description.value? description.value : '') + '</textarea></label></div>');
				break;

				case 'select': {
					let html = '<div><label>' + (description.label? '<span>' + description.label + '</span>' : '') + '<select' + line + '>';
					for(const key in description.values) {
						html += '<option value="' + description.values[key].value + '">' + description.values[key].text + '</option>';
					}
					html += '</select></label></div>';
					formEl = utils.createElementFromHTML(html);
				}
				break;

				case 'radio': {
					let html = '<div class="radioContainer">' + (description.label? '<span>' + description.label + '</span>' : '') + '<br />';
					for(const key in description.values) {
						html += '<label><input type="radio"' + line + ' value="' + description.values[key].value + '" />' + description.values[key].text + '</label> ';
					}
					html += '</div>';
					formEl = utils.createElementFromHTML(html);
				}
				break;

				case 'checkbox': {
					let html = '<div class="radioContainer">' + (description.label? '<span>' + description.label + '</span>' : '') + '<br />';
					html += '<label><input type="checkbox"' + line + ' />' + description.text + '</label> ';
					html += '</div>';
					formEl = utils.createElementFromHTML(html);
				}
				break;

				default:
				formEl = utils.createElementFromHTML('<div><label>' + (description.label? '<span>' + description.label + '</span>' : '') + '<input type="' + description.type + '"' + line + ' /></label></div>');
			}

			formEl.addEventListener('click', (event) => {
				event.stopPropagation();
			}, false);
			formObject.appendChild(formEl);
		}

		const makeFormData = (formObject) => {
			const formData = {};
			const inputs = Array.from(formObject.getElementsByTagName('input')).concat(Array.from(formObject.getElementsByTagName('select'))).concat(Array.from(formObject.getElementsByTagName('textarea')));
			let valid = true;
			for(let i = 0, iEnd = inputs.length; i < iEnd; i++) {
				const input = inputs[i];

				if(input.name && input.value) {
					if(input.getAttribute('type') == 'datetime-local') {
						formData[input.name] = new Date(input.value).getTime()/1000;
					} else if(input.getAttribute('type') == 'file') {
						formData[input.name] = input.files[0];
					} else if(input.getAttribute('type') == 'radio') {
						if(input.checked) formData[input.name] = input.value;
					} else if(input.getAttribute('type') == 'checkbox') {
						formData[input.name] = input.checked;
					} else formData[input.name] = input.value;
				}

				input.setCustomValidity('');
				if(!input.validity.valid) {
					valid = false;
					input.setCustomValidity('Veuillez remplir correctmement ce champ');
				}
			}

			return valid? formData : false;
		};

		const buttons = [{value: 'Annuler'}, {value: 'Envoyer', action: () => { const formData = makeFormData(formObject); return (formData? callback(formData) : false); }}];

		return custom(formObject, title, buttons, '', false, 'form');
	}

	function remove(popupEl) {
		utils.removeClass(popupEl, 'in');

		setTimeout(() => {
			if(popupEl && popupEl.parentNode) {
				popupEl.parentNode.removeChild(popupEl);
			}
		}, 500);
	}

	function removeType(type) {
		const popupEls = popupContainer.children;
		for(let i = 0, iend = popupEls.length; i < iend; i++) {
			if(popupEls[i].getAttribute('data-type') === type) {
				remove(popupEls[i]);
			}
		}
	}

	function removeAll() {
		const popupEls = popupContainer.children;
		for(let i = 0, iend = popupEls.length; i < iend; i++) {
			remove(popupEls[i]);
		}
	}
	// ---------- !Scope Privé ----------

	// ---------- Init ----------
	document.body.appendChild(popupContainer);
	// ---------- !Init ----------

	// ---------- Scope Public ----------
	return {
		'custom': custom,
		'ask': ask,
		'wait': wait,
		'success': success,
		'error': error,
		'fatal': fatal,
		'form': form,
		'remove': remove,
		'removeType': removeType,
		'removeAll': removeAll,
		'DEFAULT': DEFAULT
	}
	// ---------- !Scope Public ----------
})();
