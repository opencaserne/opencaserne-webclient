'use strict';

const RestapiClient = (apiUrl, restOptions) => {

	restOptions = restOptions || {};

	// DATA
	let apiKey = null;
	let baseLinks = null;
	let authLinks = null;

	let context = {};

	// SESSION TOKEN
	const setApiKey = (st) => {

		apiKey = st;

		if(apiKey === null) {

			localStorage.removeItem('rest_apiKey');

			if(typeof restOptions.onrestdisconnected === 'function') {
				try {
					restOptions.onrestdisconnected(context);
				} catch(err) {
					console.error('onrestdisconnected', err);
				}
			}
			return;
		}

		localStorage.setItem('rest_apiKey', st);

		if(typeof restOptions.onrestconnected === 'function') {
			try {
				restOptions.onrestconnected(context);
			} catch(err) {
				console.error('onrestconnected', err);
			}
		}
	};

	// INIT LINKS
	const initBaseLinks = () => {
		return httpRequest.get(apiUrl)
		.then((req) => {
			baseLinks = req.responseJSON;
		})
		.catch((err) => {
			popup.error('La connexion au service à rencontrée un problème');
			return Promise.reject(err);
		});
	};

	const restRequest = (link, data, options) => {

		options = options || {};

		data = data || {};
		data.body = data.body || {};

		if(link.auth == 'session') {
			if(apiKey) {
				if(!options.headers) options.headers = [];
				options.headers.push(['Authorization', 'bearer ' + apiKey]);
			} else {

				return Promise.reject({
					errorCode: 401,
					errorMessage: 'No authorization header'
				});
			}
		}

		return httpRequest.request(link.method, apiUrl + link.href, data, options)
		.then((request) => {

			if(link.isAuthRoute && request.responseJSON && request.responseJSON.apiKey) {
				setApiKey(request.responseJSON.apiKey);
				authLinks = request.responseJSON.links;
			}

			return request;
		})
		.catch((err) => {

			if(err.errorCode === 401) {
				setApiKey(null);
			}

			return Promise.reject(err);
		});
	};

	return initBaseLinks().then(() => {

		context = {
			isAuthentified: () => { return apiKey !== null && authLinks; },
			baseLinks: () => { return baseLinks; },
			authLinks: () => { return authLinks; },
			logout: () => { setApiKey(null); },
			setOptions: (options) => { restOptions = options; },
			request: restRequest
		};

		setApiKey(localStorage.getItem('rest_apiKey'));

		if(apiKey) {
			return restRequest({href: '/auth/links', method: 'GET', auth: 'bearer'})
			.then((request) => {
				authLinks = request.responseJSON.links;
				return context;
			})
			.catch((err) => {
				setApiKey(null);
				return context;
			});
		} else {
			return context;
		}
	});
};
