const utils = {

	/* ClassName*/
	containsClass: function(el, className) {
		return el.classList.contains(className);
	},
	addClass: function(el, className) {
		el.classList.add(className);
	},
	removeClass: function(el, className) {
		el.classList.remove(className);
	},
	toogleClass: function(el, className) {
		el.classList.toogle(className);
	},
	/* !ClassName */

	getUrlParam: function(key, def) {

		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		const regex = new RegExp('[\\?&]' + key + '=([^&#]*)');
		const qs = regex.exec(window.location.href);
		if(qs == null) {
			return def;
		}
		else {
			return qs[1];
		}
	},

	dispatchEvent: function(target, eventName) {
		const event = document.createEvent('Event');
		event.initEvent(eventName, true, true);
		target.dispatchEvent(event);
	},

	playCssAnimationByClass(target, className) {
		utils.removeClass(target, className);
		void target.offsetWidth; // Reflow
		utils.addClass(target, className);
	},

	createElementFromHTML(html) {
		const div = document.createElement('div');
		div.innerHTML = html;

		return div.firstChild;
	},

	dateModelShort:'%j %F %Y',
	dateModel: '%l %j %F %Y à %Gh%i',
	dateModelSeconds :'%j %F %Y à %G:%i:%s',
	month: ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'],
	day: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
	dateToString: function(model, date){
		model = model
		.replace('%Y', date.getFullYear())
		.replace('%y', (''+date.getFullYear()).slice(-2))
		.replace('%F', utils.month[date.getMonth()])
		.replace('%m', ('0'+(date.getMonth()+1)).slice(-2))
		.replace('%n', date.getMonth()+1)
		.replace('%j', date.getDate())
		.replace('%l', utils.day[date.getDay()])
		.replace('%G', date.getHours())
		.replace('%i', ('0'+date.getMinutes()).slice(-2))
		.replace('%s', ('0'+date.getSeconds()).slice(-2));

		return model;
	},
	isSameDay: function(firstDate, secondDate) {
		return firstDate.getFullYear() === secondDate.getFullYear() && firstDate.getMonth() === secondDate.getMonth() && firstDate.getDate() === secondDate.getDate();
	},

	arrayToObject: function(key, array) {
		const object = {};

		for(let i in array) {
			const entry = array[i];
			object[entry[key]] = entry;
		}

		return object;
	},

	uriSerialize: function(obj) {
		const str = [];

		for (var p in obj) {
			if (obj.hasOwnProperty(p)) {
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
			}
		}

		return str.join("&");
	},

	addTimedEventListener: function(el, eventName, callback, delay) {

		let timeout;
		el.addEventListener(eventName, (e) => {
			if(timeout) clearTimeout(timeout);
			timeout = setTimeout(callback, delay, e);
		}, false);
	},

	sortByKey: function(array, key, asc) {

		if(asc === false) asc = 1;
		else asc = -1;

		array.sort((a, b) => {
			if(a[key] < b[key]) return asc;
			if(a[key] > b[key]) return -1*asc;
			return 0;
		});
	},

	getElementPosition: function(el) {

		var left = 0;
		var top = 0;
		var result = {};

		var calc = function(subEl) {
			if(!!subEl) {
				const elLeft = subEl.offsetLeft - subEl.scrollLeft;
				const elTop = subEl.offsetTop - subEl.scrollTop;
				left += elLeft;
				top += elTop;

				console.log('subEl', subEl, elLeft, elTop);

				calc(subEl.offsetParent);
			}
			else {
				result = {
					left: el.offsetLeft - el.scrollLeft + left,
					top: el.offsetTop - el.scrollTop + top
				}
			}
		};
		calc(el.offsetParent);

		return { x: result.left, y: result.top, width: el.offsetWidth, height: el.offsetHeight};
	},

	smoothScroll: (() => {

		let scrollInterval = null;

		return (targetEl, container) => {

			if( scrollInterval !== null ) {
				clearInterval( scrollInterval );
			}

			if(!targetEl) {
				return;
			}

			const targetTop = targetEl.offsetTop + targetEl.parentNode.offsetTop;

			let lastContainerTop = null;
			scrollInterval = setInterval(() => {
				let containerTop = container.scrollTop;
				let distance = (targetTop - containerTop)*0.04;
				distance = (distance >= 0)? Math.ceil(distance) : Math.floor(distance);

				container.scrollTop = containerTop + distance;

				if(Math.abs(targetTop - containerTop) <= 1 || (lastContainerTop && Math.abs(lastContainerTop - containerTop) === 0)) {
					clearInterval(scrollInterval);
					container.scrollTop = targetEl.offsetTop + targetEl.parentNode.offsetTop;
				}

				lastContainerTop = containerTop;
			}, 10);
		};
	})()
};


/* ClassName SHIM */
if(typeof document === 'undefined' || !('classList' in document.createElement('a'))) {
	utils.containsClass = function(el, className) {
		return el.className.split(' ').indexOf(className) > -1;
	};
	utils.addClass = function(el, className) {
		if(!utils.containsClass(el, className)) el.className += ' ' + className;
	};
	utils.removeClass = function(el, className) {
		if(utils.containsClass(el, className)) {
			const arrayClass = el.className.split(' ');
			arrayClass.splice(arrayClass.indexOf(className), 1);
			el.className = arrayClass.join(' ');

			utils.removeClass(el, className);
		}
	};
	utils.toogleClass = function(el, className) {
		if(utils.containsClass(el, className)) {
			utils.removeClass(el, className);
		}
		else {
			utils.addClass(el, className);
		}
	};
}
/* !ClassName SHIM */
