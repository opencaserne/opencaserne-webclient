'use strict';

const MyApp = function(contentElement, routes, appOptions) {
	// ---------- Scope Privé ----------
	let mXhrRouteViewReq,
	mRouteId;

	let context;

	const debug = true;

	window.onpopstate = function(evt) {
		console.log('click', evt.state);

		const routeId = getUrlRouteId(false);
		changeRoute(routeId, evt.state, true);
	};

	function getUrlRouteId(defaultRouteId) {
		let routeId = utils.getUrlParam('route');

		if(!routes[routeId]) {
			routeId = document.location.pathname.substring(1);
		}

		if(!routes[routeId]) {
			routeId = defaultRouteId;
		}

		return routeId;
	}

	function changeRoute( routeId, data, isBack ) {
		data = data || {};

		if(mXhrRouteViewReq) {
			console.log('abort', routeId);
			mXhrRouteViewReq.abort();
			mXhrRouteViewReq = null;
		}

		const route = routes[routeId];
		if(!route) {
			return Promise.reject({ message: 'La page n\'existe pas' });
		}
		else if(navigator.onLine === false && !route.offlineAccess) {
			return Promise.reject({ message: 'Vous n\'êtes pas connecté à Internet'});
		}

		if(typeof route.hook === 'function') { // S'il y a un hook, on le réalise
		const hook = route.hook(context);
		if(hook !== true) { // S'il ne retourne pas exactement true, on annule le chargement et retourne la promise du hook
		return hook;
	}
}

if(route.url) { // Si une url est spécifiée, on change d'url et arrête le traitement
window.location.href = route.url;
return;
}

return loadRoute(routeId, route, data, isBack);
}

function setLoad(coeff) {
	contentElement.className = 'load' + (Math.round(coeff*10)*10);
	contentElement.setAttribute('data-load', (coeff*100)+'%');
}

function loadRoute( routeId, route, data, isBack ) {
	let isAborted = false;
	let successDecrement = {};
	let total = 0;
	let view = '';

	setLoad(0.1);
	const slowTimeout = setTimeout(function() {
		utils.addClass(contentElement, 'slow');
	}, 2000);

	return new Promise((resolve, reject) => {

		/* * *
		*  Traitement des retours
		* * */
		const successCallback = function(type, xhrOrEvent) {
			if(isAborted) return;

			successDecrement[ type ]--;

			let rest = 0;
			for(const i in successDecrement) {
				rest = rest + successDecrement[i];
			}
			const fait = total-rest;
			setLoad(0.1 + fait/total*0.8);

			if(type === 'VIEW') {
				const req = xhrOrEvent;
				view = req.responseText;
			}

			if(!rest) {
				clearTimeout(slowTimeout);
				applyRoute(routeId, route, view, data, isBack);
				resolve();
			}
		};
		const errorCallback = function(type, xhrOrEvent) {
			if(isAborted) return;
			isAborted = true;
			reject();

			setLoad(0);
			clearTimeout(slowTimeout);
			utils.removeClass(contentElement, 'slow');

			const popupButtons = [
				{ 'value': 'OK' },
				{ 'value': 'Réessayer', 'action': 'myApp.then((app) => app.changeRoute(\'' + routeId + '\'));' }
			];

			if(type === 'VIEW' ) {
				const req = xhrOrEvent;

				if(req.status === 0) {
					popup.error('Vérifiez votre connexion Internet', 'La page n\'a pu être chargée', popupButtons);
					return;
				} else if(req.status >= 500 && req.status < 600) {
					popup.error('Le serveur a rencontré une erreur', 'La page n\'a pu être chargée', popupButtons);
					return
				}
			}

			popup.error('Une erreur s\'est produite', 'La page n\'a pu être chargée', popupButtons);
		};

		/* * *
		*  Récupération des éléments de la route
		* * */
		const hasStyle = route.style && route.style.length;
		const hasScript = route.script && route.script.length;
		const hasView = route.view && true;

		if( hasStyle || hasScript || hasView ) {

			if(hasStyle) { // Si un ou des css sont spécifiés
				successDecrement['STYLE'] = route.style.length;
				for(const i in route.style) {
					total++;
					loadStyle(route.style[i], successCallback, errorCallback);
				}
			}
			if(hasScript) { // Si un ou des js sont spécifiés
				successDecrement['SCRIPT'] = route.script.length;
				for(const i in route.script) {
					total++;
					loadScript(route.script[i], successCallback, errorCallback);
				}
			}
			if(hasView) { // Si une vue est spécifiée
				successDecrement['VIEW'] = 1;
				total++;
				loadView(route.view, successCallback, errorCallback);
			}

		}
		else {
			clearTimeout(slowTimeout);
			applyRoute(routeId, route, view, data, isBack);
			resolve();
		}
	});
}

function loadStyle( url, successCallback, errorCallback ) {
	const styleEl = document.createElement('link');
	styleEl.setAttribute('rel', 'stylesheet')
	styleEl.setAttribute('type', 'text/css')
	styleEl.setAttribute('href', url + (debug? '?'+(new Date().getTime()) : ''));

	styleEl.onload = successCallback.bind(this, 'STYLE');
	styleEl.onerror = errorCallback.bind(this, 'STYLE');

	document.body.appendChild(styleEl);
}

function loadScript( url, successCallback, errorCallback ) {
	const scriptEl = document.createElement('script');
	scriptEl.setAttribute('src', url + (debug? '?'+(new Date().getTime()) : ''));

	scriptEl.onload = successCallback.bind(this, 'SCRIPT');
	scriptEl.onerror = errorCallback.bind(this, 'SCRIPT');

	document.body.appendChild(scriptEl);
}

function loadView( url, successCallback, errorCallback ) {

	const req = new XMLHttpRequest();
	mXhrRouteViewReq = req;
	req.open('GET', url + (debug? '?'+(new Date().getTime()) : ''));
	req.onreadystatechange = function (evt) {
		if (req.readyState == XMLHttpRequest.DONE) {// Requête terminée
			mXhrRouteViewReq = null;

			if(req.status >= 200 && req.status < 300) {// HTTP OK
				successCallback('VIEW', req);
			}
			else { // Si la requête n'a pas été terminée correctement
			errorCallback('VIEW', req)
		}
	}
};
req.send();
}

function applyRoute( routeId, route, view, data, isBack ) {
	contentElement.innerHTML = view;
	document.title = route.title + (appOptions.titleSuffix? appOptions.titleSuffix : '');
	mRouteId = routeId;
	contentElement.setAttribute('data-page', routeId);

	setLoad(1);
	utils.removeClass(contentElement, 'slow');

	const uriData = {};
	location.search.substring(1).replace(/([^=&]+)=([^&]*)/g, function(m, key, value) {
		uriData[decodeURIComponent(key)] = decodeURIComponent(value);
	});

	if(typeof appOptions.onroutechange === 'function') {
		try {
			appOptions.onroutechange(context, routeId, route, route.data || [], data, uriData);
		}
		catch(err) {
			console.error('onroutechange', err);
		}
	}

	if(typeof route.action === 'function') {
		try {
			route.action(context, routeId, route, route.data || [], data, uriData);
		}
		catch(err) {
			console.error(err);
		}
	}

	if(!isBack && !route.sameUrl) {
		history.pushState(data, document.title, '?route='+routeId);
	}

	setTimeout(function() { setLoad(0); }, 200);
}

function setOptions(options) {
	Object.assign(appOptions, options);
}
// ---------- !Scope Privé ----------


// ---------- Init ----------
routes = routes || {};
appOptions = appOptions || {};
// ---------- !Init ----------


// ---------- Scope Public ----------
context = {
	getUrlRouteId: getUrlRouteId,
	changeRoute: changeRoute,
	setOptions: setOptions
}

return context;
// ---------- !Scope Public ----------
};
