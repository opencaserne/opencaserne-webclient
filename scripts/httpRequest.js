'use strict';

const httpRequest = {

	request: function(method, url, data, options) {

		return new Promise((resolve, reject) => {

			data = data || {};
			let query = data.query;
			let body = data.body;
			options = options || {};

			const req = new XMLHttpRequest();

			if(query) {
				url = url + (url.indexOf('?') >= 1? '&' : '?') + utils.uriSerialize(query);
			}
			req.open(method, url, true);

			if(options.headers) {
				for(const key in options.headers) {

					const header = options.headers[key];
					req.setRequestHeader(header[0], header[1]);
				}
			}

			let fd = null;
			if(body) {
				if(options.multipart === 'form-data') {
					fd = new FormData();
					for(const key in body) {
						fd.append(key, body[key]);
					}
				} else {
					fd = JSON.stringify(body);
					req.setRequestHeader('Content-Type', 'application/json');
				}
			}

			req.onreadystatechange = function(event) {

				if(req.readyState !== XMLHttpRequest.DONE) {
					return;
				}

				if(req.status != 204) {
					const responseContentType = req.getResponseHeader('content-type');
					if(responseContentType && responseContentType.indexOf('application/json') > -1) {

						try {
							req.responseJSON = JSON.parse(req.responseText);
						} catch(e) {

							reject({
								message: {
									default: 'A communication error has occurred',
									en: 'A communication error has occurred',
									fr: 'Une erreur de communication s\'est produite',
									description: '#httpRequest_json_bad_format'
								}
							});
							return;
						}
					}
				}

				if(req.status < 200 || req.status > 299) {
					const error = {
						status: req.status,
						message: (req.responseJSON && (req.responseJSON.message || req.responseJSON.error)? req.responseJSON.message || req.responseJSON.error : (req.status > 0? {
							default: 'A remote network error occurred',
							en: 'A remote network error occurred',
							fr: 'Une erreur réseau distante s\'est produite',
							description: '#httpRequest_network_status_' + req.status
						} : {
							default: 'An unknown network error occurred',
							en: 'An unknown network error occurred',
							fr: 'Une erreur réseau inconnue s\'est produite',
							description: '#httpRequest_network_status_' + req.status
						})),
					};
					reject(error);
					return;
				}

				resolve(req);
			};

			req.send(fd);
		});
	},
	get: function(url, data, options) {
		return httpRequest.request('GET', url, data, options);
	},
	post: function(url, data, options) {
		return httpRequest.request('POST', url, data, options);
	},
	put: function(url, data, options) {
		return httpRequest.request('PUT', url, data, options);
	},
	delete: function(url, data, options) {
		return httpRequest.request('DELETE', url, data, options);
	}
};
